const uniqueId = require('lodash/uniqueId')
const find = require('lodash/find')
const forEach = require('lodash/forEach')

let games = []

function createGame (req) {
  let {playerName, startRows} = req.payload
  let gameId = uniqueId('game_')
  let socket = req.plugins['hapi-io'].socket
  let playerId = socket.id

  games.push({
    gameId: gameId,
    startRows: startRows,
    players: [{
      playerId: playerId,
      playerName: playerName,
      socket: socket
    }]
  })

  socket.emit('game-created', {
    status: 'success',
    playerId: playerId,
    gameId: gameId,
    startPosition: 'top'
  })
}

function connectGame (req) {
  let {gameId, playerName} = req.payload
  let game = find(games, {gameId: gameId})
  let socket = req.plugins['hapi-io'].socket
  let playerId = socket.id
  if (game) {
    let players = game.players
    if (players.length >= 2) {
      socket.emit('game-error', {
        message: 'game is full'
      })
    } else {
      let enemy = players[0]

      players.push({
        playerId: playerId,
        playerName: playerName,
        socket: socket
      })

      socket.emit('game-connected', {
        status: 'success',
        playerId: playerId,
        gameId: gameId,
        enemyName: enemy.playerName,
        startRows: game.startRows,
        startPosition: players.length === 2 ? 'bottom' : 'top'
      })

      game.players[0].socket.emit('enemy-connected', {
        enemyName: playerName
      })
    }
  } else {
    socket.emit('game-error', {
      message: `No game found with playerId: ${gameId}`
    })
  }
}

function playerMove (req) {
  let {playerId, xCoord, yCoord, gameId} = req.payload
  let game = find(games, {gameId: gameId})
  if (game) {
    let player = find(game.players, {playerId: playerId})
    if (player) {
      let enemy = find(game.players, player => player.playerId !== playerId)
      enemy.socket.emit('enemy-moved', {x: xCoord, y: yCoord})
    }
  }
}

function startGame (req) {
  let {gameId} = req.payload
  let game = find(games, {gameId: gameId})
  if (game) {
    forEach(game.players, player => {
      player.socket.emit('game-started')
    })
  }
}

function endGame (req) {
  let {playerId, gameId} = req.payload
  let game = find(games, {gameId: gameId})
  if (game) {
    let player = find(game.players, {playerId: playerId})
    if (player) {
      let enemy = find(game.players, player => player.playerId !== playerId)
      enemy.socket.emit('enemy-won')
    }
  }
}

module.exports = {
  createGame,
  connectGame,
  playerMove,
  startGame,
  endGame
}
