import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducers/index'

import thunk from 'redux-thunk'
import createLogger from 'redux-logger'

const logger = createLogger({
  actionTransformer: (action) => ({
    ...action,
    type: String(action.type)
  })
})

const middlewares = process.env.NODE_ENV === 'production' ? [ thunk ] : [ thunk, logger ]

export default createStore(rootReducer, applyMiddleware(...middlewares))
