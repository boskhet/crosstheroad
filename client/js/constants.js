export const UPDATE_GLOBAL = Symbol('UPDATE_GLOBAL')
export const RESET_GLOBAL = Symbol('RESET_GLOBAL')

export const UPDATE_PLAYER = Symbol('UPDATE_PLAYER')
export const RESET_PLAYER = Symbol('RESET_PLAYER')
export const SET_PLAYER_DEFAULT_POSITION = Symbol('SET_PLAYER_DEFAULT_POSITION')

export const UPDATE_ENEMY = Symbol('UPDATE_ENEMY')
export const RESET_ENEMY = Symbol('RESET_ENEMY')
export const SET_ENEMY_DEFAULT_POSITION = Symbol('SET_ENEMY_DEFAULT_POSITION')

export const UPDATE_TRAINS = Symbol('UPDATE_TRAINS')

export const NUM_ROWS = 14
export const SPEED = 100

export const CANVAS_WIDTH = 320
export const CANVAS_HEIGHT = 320
