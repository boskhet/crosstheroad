import xtag from 'x-tag'

import each from 'lodash/each'
import isUndefined from 'lodash/isUndefined'

import { Subject, Observable } from 'rxjs/Rx'

import storeObservable from './store-observable'
import { gameStartedObservable } from './game-observable'

import {
  isntGameOver
} from './reducers/game'

import {
  CANVAS_WIDTH,
  CANVAS_HEIGHT
} from './constants'

const xGame = xtag.register('x-game', {
  content: '<canvas id="game-canvas" />',
  lifecycle: {
    inserted () {
      let that = this

      const pauser = new Subject()

      that.xtag.canvas = that.querySelector('#game-canvas')
      that.xtag.ctx = that.xtag.canvas.getContext('2d')

      that._applyDefaultContentAttributes(that.xtag.canvas, {
        width: CANVAS_WIDTH,
        height: CANVAS_HEIGHT,
        style: {
          zIndex: 8,
          position: 'absolute',
          border: '1px solid'
        }
      })

      const pausableStoreObservable = pauser
        .switchMap(paused => paused ? Observable.never() : storeObservable)

      pausableStoreObservable.next(true)
      gameStartedObservable.subscribe(() => pausableStoreObservable.next(false))

      pausableStoreObservable
        .filter(isntGameOver)
        .distinctUntilChanged()
        .subscribe(that.renderScene.bind(that))
    }
  },
  methods: {
    renderScene ({ game, player, enemy, trains }) {
      this._clearScene({ game })
      this._renderPlayer({ player })
      this._renderEnemy({ player, enemy })
      this._renderTrainRows({ trains })
    },

    _applyDefaultContentAttributes (canvas, options) {
      canvas.width = options.width
      canvas.height = options.height
      canvas.style = options.style
    },

    _clearScene ({ game }) {
      const {
        defaultFieldColor
      } = game

      this.xtag.ctx.fillStyle = defaultFieldColor
      this.xtag.ctx.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)
    },

    _renderEnemy ({ player, enemy }) {
      if (!isUndefined(enemy.x) || !isUndefined(enemy.y)) {
        this.xtag.ctx.fillStyle = enemy.color
        this.xtag.ctx.fillRect(enemy.x, enemy.y, player.size, player.size)
      }
    },

    _renderPlayer ({ player }) {
      if (!isUndefined(player.x) || !isUndefined(player.y)) {
        this.xtag.ctx.fillStyle = player.color
        this.xtag.ctx.fillRect(player.x, player.y, player.size, player.size)
      }
    },

    _renderTrainRowleft (trainRow, coachSize) {
      let that = this

      each(trainRow.trains, train => {
        that.xtag.ctx.fillStyle = train.color
        if (train.endSize) {
          that.xtag.ctx.fillRect(CANVAS_WIDTH - train.endSize, train.y, train.endSize, coachSize)
          that.xtag.ctx.fillRect(0, train.y, train.startSize, coachSize)
        } else {
          that.xtag.ctx.fillRect(train.x - coachSize * train.size, train.y, coachSize * train.size, coachSize)
        }
      })
    },

    _renderTrainRowright (trainRow, coachSize) {
      let that = this
      each(trainRow.trains, train => {
        that.xtag.ctx.fillStyle = train.color
        if (train.startSize) {
          that.xtag.ctx.fillRect(train.x, train.y, train.endSize, coachSize)
          that.xtag.ctx.fillRect(0, train.y, train.startSize, coachSize)
        } else {
          that.xtag.ctx.fillRect(train.x, train.y, coachSize * train.size, coachSize)
        }
      })
    },

    _renderTrainRows ({ trains }) {
      let that = this

      each(trains.trainRows, (trainRow) => {
        that[`_renderTrainRow${trainRow.direction}`](trainRow, trains.coachSize)
      })
    }
  }
})

export default xGame
