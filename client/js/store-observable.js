import { Observable } from 'rxjs/Rx'
import store from './store'

export default Observable.from(store)
