import _ from 'lodash'
import store from './store'

import { CANVAS_WIDTH } from './constants'

export default function createStartTrainRows (number) {
  let rows = []
  _.times(number, index => {
    rows.push(createTrainRow(index))
  })
  return rows
}

function createTrainRow (index) {
  const { trains } = store.getState()
  const { minCoachNum, minGap, coachSize, maxGap, trainColors } = trains

  let leftWidth = CANVAS_WIDTH
  let _trains = []
  let speed = _.random(200, 1000)
  let startPosition = 0
  let direction = _.random(0, 1) ? 'right' : 'left'

  while (leftWidth > 0) {
    let minWidth = minCoachNum * coachSize + minGap * coachSize
    let train = createTrain(index, startPosition, trainColors, minCoachNum, coachSize)
    let gap = _.random(minGap, maxGap)

    if (leftWidth < minWidth) {
      leftWidth = 0
    } else if (leftWidth < train.size * coachSize) {
      train.size = minCoachNum
      _trains.push(train)
      leftWidth = 0
    } else {
      let size = (train.size + gap) * coachSize
      startPosition += size
      leftWidth -= size
      _trains.push(train)
    }
  }

  return {
    speed: speed,
    trains: _trains,
    direction: direction
  }
}

function createTrain (index, startPosition, trainColors, minCoachNum, coachSize) {
  return {
    color: trainColors[_.random(0, trainColors.length - 1)],
    size: _.random(minCoachNum, 6),
    x: startPosition,
    y: coachSize + index * coachSize
  }
}
