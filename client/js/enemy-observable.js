import { Observable } from 'rxjs/Rx'
import socket from './socket'

import store from './store'

import storeObservable from './store-observable'

import {
  NUM_ROWS
} from './constants'

import {
  updateEnemy
} from './reducers/enemy'

import {
  updateGlobal
} from './reducers/game'

const enemyWonObservable = Observable.fromEvent(socket, 'enemy-won')
const enemyConnectedObservable = Observable.fromEvent(socket, 'enemy-connected')
const enemyMovedObservable = Observable.fromEvent(socket, 'enemy-moved')

enemyConnectedObservable
  .withLatestFrom(storeObservable)
  .subscribe(([data, state]) => {
    const { enemyName } = data
    const { game, player } = state

    store.dispatch(updateEnemy({
      x: game.startX,
      y: player.size * (NUM_ROWS + 1),
      defaultX: game.startX,
      defaultY: player.size * (NUM_ROWS + 1),
      color: 'green',
      name: enemyName
    }))
  })

enemyMovedObservable
  .subscribe((enemy) => {
    store.dispatch(updateEnemy(enemy))
  })

enemyWonObservable
  .subscribe(() => {
    store.dispatch(updateGlobal({
      enemyWon: true
    }))
  })

export {
  enemyWonObservable,
  enemyConnectedObservable,
  enemyMovedObservable
}
