import {
  UPDATE_ENEMY,
  RESET_ENEMY,
  SET_ENEMY_DEFAULT_POSITION
} from '../constants'

const initialState = {
  size: 16,
  color: '#ff0000',
  name: null,
  x: null,
  y: null,
  defaultX: null,
  defaultY: null
}

export function updateEnemy (payload) {
  return {
    type: UPDATE_ENEMY,
    payload
  }
}

export function setDefaultPosition () {
  return {
    type: SET_ENEMY_DEFAULT_POSITION
  }
}

export default function enemy (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case UPDATE_ENEMY:
      return {
        ...state,
        ...payload
      }

    case SET_ENEMY_DEFAULT_POSITION:
      return {
        ...state,
        x: state.defaultX,
        y: state.defaultY
      }

    case RESET_ENEMY:
    default:
      return state
  }
}
