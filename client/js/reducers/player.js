import {
  UPDATE_PLAYER,
  RESET_PLAYER,
  SET_PLAYER_DEFAULT_POSITION
} from '../constants'

const initialState = {
  size: 16,
  color: '#ff0000',
  name: null,
  x: null,
  y: null,
  defaultX: null,
  defaultY: null
}

export function updatePlayer (payload) {
  return {
    type: UPDATE_PLAYER,
    payload
  }
}

export function setDefaultPosition () {
  return {
    type: SET_PLAYER_DEFAULT_POSITION
  }
}

export default function player (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case UPDATE_PLAYER:
      return {
        ...state,
        ...payload
      }

    case SET_PLAYER_DEFAULT_POSITION:
      return {
        ...state,
        x: state.defaultX,
        y: state.defaultY
      }

    case RESET_PLAYER:
    default:
      return state
  }
}
