import { combineReducers } from 'redux'
import game from './game'
import player from './player'
import enemy from './enemy'
import trains from './trains'

import {
  NUM_ROWS
} from '../constants'

import some from 'lodash/some'

const rootReducer = combineReducers({
  game,
  player,
  enemy,
  trains
})

function isPlayerWin (state) {
  const { game, player } = state
  let isTop = game.startPosition === 'top'

  return isTop ? player.y > player.size * (NUM_ROWS) : player.y < player.size
}

function isAnyCollisions ({ player, trains }) {
  const { trainRows } = trains

  return some(trainRows, trainRow => {
    return some(trainRow.trains, train => {
      return _collision(player, train, trains, trainRow.direction)
    })
  })
}

function isntAnyCollisions (...args) {
  return !isAnyCollisions(...args)
}

function _collision (player, train, trains, direction) {
  let coachSize = trains.coachSize
  return direction === 'right' ? _collisionRight(train) : _collisionLeft(train)

  function _collisionRight (train) {
    return (player.x > train.x - coachSize && player.x < train.x + coachSize * train.size) &&
      (player.y > train.y - coachSize && player.y < train.y + coachSize)
  }

  function _collisionLeft (train) {
    return (player.x > train.x - coachSize * (train.size + 1) && player.x < train.x) &&
      (player.y > train.y - coachSize && player.y < train.y + coachSize)
  }
}

export {
  isAnyCollisions,
  isntAnyCollisions,
  isPlayerWin
}

export default rootReducer
