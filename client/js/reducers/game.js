import { UPDATE_GLOBAL, RESET_GLOBAL } from '../constants'

const initialState = {
  playerId: null,
  startPosition: null,
  gameId: null,
  playerNameInput: null,
  playerWin: false,
  enemyWon: false,
  gameIdInput: null,
  defaultFieldColor: '#ffffff'
}

function updateGlobal (payload) {
  return {
    type: UPDATE_GLOBAL,
    payload
  }
}

function isGameOver ({ game }) {
  return game.playerWin || game.enemyWon
}

function isntGameOver (...args) {
  return !isGameOver(...args)
}

function isGameCreated ({ game }) {
  return game.gameId
}

function isntGameCreated (...args) {
  return !isGameCreated(...args)
}

export {
  updateGlobal,
  isGameOver,
  isntGameOver,
  isGameCreated,
  isntGameCreated
}

export default function game (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case UPDATE_GLOBAL:
      return {
        ...state,
        ...payload
      }

    case RESET_GLOBAL:
    default:
      return state
  }
}
