import {
  UPDATE_TRAINS
} from '../constants'

const initialState = {
  minGap: 3,
  maxGap: 5,
  minCoachNum: 2,
  coachSize: 16,
  trainColors: ['#f7f68e', '#ffd6a9', '#ffb9db', '#ffa9a9', '#eba9ff', '#c7a9ff', '#a9c6ff', '#b9fffd'],
  startRows: null,
  trainRows: null
}

export function updateTrains (payload) {
  return {
    type: UPDATE_TRAINS,
    payload
  }
}

export default function trains (state = initialState, action) {
  const {type, payload} = action

  switch (type) {
    case UPDATE_TRAINS:
      return {
        ...state,
        ...payload
      }

    default:
      return state
  }
}
