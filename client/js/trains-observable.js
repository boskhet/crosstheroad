import { Observable } from 'rxjs/Rx'
import map from 'lodash/map'

import store from './store'
import storeObservable from './store-observable'

import {
  CANVAS_WIDTH
} from './constants'

import { updateTrains } from './reducers/trains'

export default function (startRows) {
  return Observable // eslint-disable-line lodash/prefer-lodash-method
  .from(startRows)
  .withLatestFrom(storeObservable)
  .map(([row, state], index) => {
    let trainRow = startRows[index]

    return Observable // eslint-disable-line lodash/prefer-lodash-method
      .interval(trainRow.speed)
      .map(() => {
        map(trainRow.trains, train => moveTrain(train, trainRow.direction, state))

        return startRows
      })
  })
  .mergeAll()
  .subscribe((trainRows) => {
    store.dispatch(updateTrains({
      trainRows
    }))
  })
}

function moveTrain (train, direction, state) {
  const { trains } = state

  const coachSize = trains.coachSize
  return direction === 'right' ? moveTrainRight(train, coachSize) : moveTrainLeft(train, coachSize)
}

function moveTrainRight (train, coachSize) {
  if (train.x >= CANVAS_WIDTH) {
    train.startSize = 0
    train.endSize = 0
    train.x = 0
  } else if (train.x + train.size * coachSize >= CANVAS_WIDTH) {
    train.endSize = CANVAS_WIDTH - train.x
    train.startSize = train.size * coachSize - train.endSize
    train.x += coachSize / 2
  } else {
    train.x += coachSize / 2
  }
  return train
}

function moveTrainLeft (train, coachSize) {
  if (train.x <= 0) {
    train.startSize = 0
    train.endSize = 0
    train.x = CANVAS_WIDTH
  } else if (train.x - train.size * coachSize <= 0) {
    train.startSize = train.x
    train.endSize = train.size * coachSize - train.startSize
    train.x -= coachSize / 2
  } else {
    train.x -= coachSize / 2
  }
  return train
}
