import { Observable } from 'rxjs/Rx'

import './game-view'
import './layout-view'

import {
  NUM_ROWS,
  SPEED,

  CANVAS_WIDTH
} from './constants'

import socket from './socket'

import createTrainsObservable from './trains-observable'
import createStartTrainRows from './trains-generator'

import store from './store'
import storeObservable from './store-observable'

import {
  isAnyCollisions,
  isntAnyCollisions,
  isPlayerWin
} from './reducers/index'

import {
  updateGlobal,
  isGameCreated,
  isntGameCreated
} from './reducers/game'

import { updateEnemy } from './reducers/enemy'
import { updateTrains } from './reducers/trains'

import {
  setDefaultPosition,
  updatePlayer
} from './reducers/player'

require('../css/app.less')

import {
  gameConnectedObservable,
  gameCreatedObservable,
  gameStartedObservable,
  gameErrorObservable
} from './game-observable'

const playerNameInput = `Player${new Date().getMilliseconds()}`

const { player } = store.getState()

store.dispatch(updateGlobal({
  startX: CANVAS_WIDTH / 2 - player.size / 2,
  playerNameInput
}))

Observable
  .fromEvent(document.getElementById('connect-game-input'), 'keyup')
  .distinctUntilKeyChanged('timeStamp')
  .pluck('target', 'value')
  .debounceTime(SPEED)
  .distinctUntilChanged()
  .subscribe(value => {
    store.dispatch(updateGlobal({
      gameIdInput: value
    }))
  })

document.getElementById('player-name-input').defaultValue = playerNameInput

Observable
  .fromEvent(document.getElementById('player-name-input'), 'keyup')
  .distinctUntilKeyChanged('timeStamp')
  .pluck('target', 'value')
  .debounceTime(SPEED)
  .distinctUntilChanged()
  .startWith(playerNameInput)
  .subscribe(value => {
    store.dispatch(updateGlobal({
      playerNameInput: value
    }))
  })

gameConnectedObservable
  .withLatestFrom(storeObservable)
  .subscribe(([data, state]) => {
    const { playerId, startPosition, gameId, enemyName, startRows } = data
    const { game, player } = state

    store.dispatch(updatePlayer({
      x: game.startX,
      y: player.size * (NUM_ROWS + 1),
      defaultX: game.startX,
      defaultY: player.size * (NUM_ROWS + 1),
      color: 'green'
    }))

    store.dispatch(updateEnemy({
      x: game.startX,
      y: 0,
      defaultX: game.startX,
      defaultY: 0,
      color: '#ff0000',
      name: enemyName
    }))

    store.dispatch(updateTrains({
      startRows
    }))

    store.dispatch(updateGlobal({
      playerId,
      startPosition,
      gameId
    }))
  })

gameCreatedObservable
  .withLatestFrom(storeObservable)
  .subscribe(([data, state]) => {
    const { gameId } = data
    const { game } = state

    store.dispatch(updatePlayer({
      x: game.startX,
      y: 0,
      defaultX: game.startX,
      defaultY: 0,
      color: '#ff0000'
    }))

    store.dispatch(updateGlobal({
      startPosition: 'top',
      gameId
    }))
  })

gameStartedObservable
  .withLatestFrom(storeObservable)
  .map(([event, state]) => state)
  .subscribe(clientStartGame)

gameErrorObservable.subscribe(data => {
  console.error(data.message)
})

Observable
  .fromEvent(document.getElementById('new-game-button'), 'click')
  .withLatestFrom(storeObservable)
  .map(([event, state]) => state)
  .filter(isntGameCreated)
  .subscribe(createNewGame)

Observable
  .fromEvent(document.getElementById('connect-game-button'), 'click')
  .withLatestFrom(storeObservable)
  .map(([event, state]) => state)
  .filter(({game}) => { return game.gameIdInput })
  .subscribe(connectToLaunchedGame)

Observable
  .fromEvent(document.getElementById('start-game-button'), 'click')
  .withLatestFrom(storeObservable)
  .map(([event, state]) => state)
  .subscribe(serverStartGame)

function connectToLaunchedGame (state) {
  const { game } = state

  socket.emit('connect-game', {
    playerName: game.playerNameInput,
    gameId: game.gameIdInput
  })
}

function createNewGame (state) {
  const { game } = state

  let startRows = createStartTrainRows(NUM_ROWS)

  store.dispatch(updateTrains({
    startRows
  }))

  socket.emit('create-game', {
    playerName: game.playerNameInput,
    startRows: startRows
  })
}

function serverStartGame (state) {
  const { game } = state
  const { gameId } = game

  socket.emit('start-game', {
    gameId
  })
}

function clientStartGame (state) {
  const { trains } = state

  createTrainsObservable(trains.startRows)

  storeObservable
    .filter(isAnyCollisions)
    .subscribe(() => {
      store.dispatch(setDefaultPosition())
    })

  storeObservable
    .filter(isntAnyCollisions)
    .filter(isPlayerWin)
    .subscribe(({ game }) => {
      const { gameId, playerId } = game

      socket.emit('player-won', {
        gameId,
        playerId
      })
    })

  storeObservable
    .filter(isGameCreated)
    .distinctUntilKeyChanged('player', (previous, next) => {
      return previous.x !== next.x || previous.y !== next.y
    })
    .subscribe((state) => {
      const {
        game,
        player
      } = state

      socket.emit('player-move', {
        gameId: game.gameId,
        playerId: game.playerId,
        xCoord: player.x,
        yCoord: player.y
      })
    })
}
