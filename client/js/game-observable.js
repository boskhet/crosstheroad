import { Observable } from 'rxjs/Rx'

import socket from './socket'

const gameConnectedObservable = Observable.fromEvent(socket, 'game-connected')
const gameCreatedObservable = Observable.fromEvent(socket, 'game-created')
const gameStartedObservable = Observable.fromEvent(socket, 'game-started')
const gameErrorObservable = Observable.fromEvent(socket, 'game-error')

export {
  gameConnectedObservable,
  gameCreatedObservable,
  gameStartedObservable,
  gameErrorObservable
}
