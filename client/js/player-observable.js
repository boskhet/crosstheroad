import { Observable } from 'rxjs/Rx'
import keycode from 'keycode'

import store from './store'

import storeObservable from './store-observable'

import socket from './socket'

const playerWonObservable = Observable.fromEvent(socket, 'player-won')

import { updatePlayer } from './reducers/player'

Observable // eslint-disable-line lodash/prefer-lodash-method
  .fromEvent(document, 'keyup')
  .distinctUntilKeyChanged('timeStamp')
  .withLatestFrom(storeObservable)
  .subscribe(([event, state]) => {
    const { player } = state
    const { x, y, size } = player

    let newX = x
    let newY = y
    let canMove

    switch (keycode(event)) {
      case 'left':
        canMove = x >= 16
        newX = canMove ? x - size : 0

        break
      case 'right':
        canMove = x <= 304
        newX = canMove ? x + size : 320

        break
      case 'up':
        canMove = y >= 16
        newY = canMove ? y - size : 0

        break
      case 'down':
        canMove = y <= 304
        newY = canMove ? y + size : 320

        break
    }

    store.dispatch(updatePlayer({
      x: newX,
      y: newY
    }))
  })

export {
  playerWonObservable
}
