import xtag from 'x-tag'

import { Observable } from 'rxjs/Rx'

import {
  gameConnectedObservable,
  gameCreatedObservable,
  gameStartedObservable
} from './game-observable'

import {
  enemyWonObservable,
  enemyConnectedObservable
} from './enemy-observable'

import {
  playerWonObservable
} from './player-observable'

import {
  isntGameCreated
} from './reducers/game'

import storeObservable from './store-observable'

const xLayout = xtag.register('x-layout', {
  lifecycle: {
    inserted () {
      let that = this

      // Create aliases
      that.connectGameRow = document.getElementById('connect-game-row')
      that.newGameRow = document.getElementById('new-game-row')
      that.newGameDivider = document.getElementById('new-game-divider')
      that.startGameRow = document.getElementById('start-game-row')
      that.messageContainer = document.getElementById('message-container')
      that.gameIdRow = document.getElementById('game-id-row')
      that.enemyNameRow = document.getElementById('enemy-name-row')

      gameCreatedObservable
        .withLatestFrom(storeObservable)
        .subscribe(([data, state]) => {
          const { gameId } = data

          that._showGameIdRow(gameId)
          that._showMessage('Waiting for your opponent...')
        })

      gameConnectedObservable
        .withLatestFrom(storeObservable)
        .subscribe(([ data ]) => {
          const { enemyName } = data

          that._hideControlButtons()
          that._showEnemyRow(enemyName)
          that._showMessage('Waiting game start...')
        })

      gameStartedObservable
        .subscribe(() => {
          that._hide('start-game-row')
          that._clearMessage()
        })

      playerWonObservable
        .subscribe(() => {
          that._apply('game-end-message', '<h4>Congratz you WIN!!!!!</h4>')
          that._show('game-end-message')
        })

      enemyWonObservable
        .subscribe(({ enemy }) => {
          that._apply('game-end-message', `<h4>You lost! =(${enemy.name} won!! </h4>`)
          that._show('game-end-message')
        })

      enemyConnectedObservable
        .withLatestFrom(storeObservable)
        .subscribe(([data, state]) => {
          const { enemyName } = data

          that._showEnemyRow(enemyName)
          that._showStartGameButton()
          that._showMessage('Waiting game start...')
        })

      Observable
        .fromEvent(document.getElementById('new-game-button'), 'click')
        .withLatestFrom(storeObservable)
        .map(([event, state]) => state)
        .filter(isntGameCreated)
        .subscribe(() => {
          that._hideControlButtons()
        })
    }
  },
  methods: {
    _hideControlButtons () {
      this._hide(this.newGameRow)
      this._hide(this.newGameDivider)
      this._hide(this.connectGameRow)
    },

    _showEnemyRow (name) {
      let text = document.createTextNode(name)
      let span = document.createElement('span')
      span.appendChild(text)
      this.enemyNameRow.appendChild(span)
      this._show(this.enemyNameRow, 'flex')
    },

    _showGameIdRow (gameId) {
      let text = document.createTextNode(gameId)     // Create a text node
      let span = document.createElement('span')
      span.appendChild(text)
      this.gameIdRow.appendChild(span)
      this._show(this.gameIdRow, 'flex')
    },

    _showStartGameButton () {
      this._show(this.startGameRow)
    },

    _hide (element) {
      let style = element.style || document.getElementById(element).style
      style.display = 'none'
    },

    _show (element, display) {
      let style = element.style || document.getElementById(element).style
      style.display = display || 'block'
    },

    _apply (elementId, content) {
      let element = document.getElementById(elementId)
      element.innerHTML = content
    },

    _showMessage (message) {
      this._clearMessage()
      this.messageContainer.innerHTML = message
    },

    _clearMessage () {
      this.messageContainer.innerHTML = ''
    }
  }
})

export default xLayout
