var path = require('path')
var WebpackDevServer = require('webpack-dev-server')
var webpack = require('webpack')
var config = require('./webpack.config')

var compiler = webpack(config)
var server = new WebpackDevServer(compiler, {
  contentBase: path.join(__dirname, '/public'),
  hot: true,
  noInfo: false,
  filename: 'bundle.js',
  publicPath: config.output.publicPath,
  historyApiFallback: {
    index: 'index.html'
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  stats: { colors: true }
})

server.listen(1337, 'localhost', function (err) {
  if (err) {
    console.log(err)
    return
  }

  console.log('Listening at http://localhost:1337')
})
