var path = require('path')
var webpack = require('webpack')
// var nodeExternals = require('webpack-node-externals')

module.exports = {
  devtool: 'eval',
  entry: [
    'webpack/hot/dev-server',
    path.join(__dirname, '/client/js/index.js')
  ],
  output: {
    path: path.join(__dirname, 'public/assets'),
    filename: 'bundle.js',
    publicPath: '/public/assets'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  stats: {
    colors: true
  },
  watchOptions: {
    poll: true // http://andrewhfarmer.com/webpack-watch-in-vagrant-docker/
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel', // 'babel-loader' is also a legal name to reference
        query: {
          presets: ['es2015', 'stage-0'],
          cacheDirectory: true
        }
      },
      {
        test: /\.less/,
        loader: 'style!css!less'
      },
      {
        test: /\.json$/,
        exclude: /node_modules/,
        loader: 'json'
      }
    ]
  }
}
