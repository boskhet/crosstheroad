var path = require('path')
var webpack = require('webpack')
// var nodeExternals = require('webpack-node-externals')

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [ path.join(__dirname, '/client/js/index.js') ],
  output: {
    path: path.join(__dirname, 'public/assets'),
    filename: 'bundle.js',
    publicPath: '/public/'
  },
  // target: 'node',
  // externals: ['ws'],
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel', // 'babel-loader' is also a legal name to reference
        query: {
          presets: ['es2015', 'stage-0']
        }
      },
      { test: /\.less/, loader: 'style!css!less' }
    ]
  }
}
