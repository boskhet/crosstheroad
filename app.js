'use strict'

const game = require('./server/game.js')
const Hapi = require('hapi')

// Create a server with a host and port
const server = new Hapi.Server({})

server.connection({
  host: 'localhost',
  port: 3300
})

server.register(require('inert'), (err) => {
  if (err) {
    throw err
  }

  // static routes
  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: (req, res) => {
      res.file('./public/index.html')
    }
  })

  server.route({
    method: 'GET',
    path: '/public/assets/bundle.js',
    handler: (req, res) => {
      res.file('./public/assets/bundle.js')
    }
  })

  server.route({
    method: 'POST',
    path: '/api/player/move',
    config: {
      plugins: {
        'hapi-io': {
          event: 'player-move'
        }
      }
    },
    handler: req => {
      game.playerMove(req)
    }
  })

  server.route({
    method: 'POST',
    path: '/api/game/create',
    config: {
      plugins: {
        'hapi-io': {
          event: 'create-game'
        }
      }
    },
    handler: req => {
      game.createGame(req)
    }
  })

  server.route({
    method: 'POST',
    path: '/api/game/connect',
    config: {
      plugins: {
        'hapi-io': {
          event: 'connect-game'
        }
      }
    },
    handler: req => {
      game.connectGame(req)
    }
  })

  server.route({
    method: 'POST',
    path: '/api/game/start',
    config: {
      plugins: {
        'hapi-io': {
          event: 'start-game'
        }
      }
    },
    handler: req => {
      game.startGame(req)
    }
  })

  server.route({
    method: 'POST',
    path: '/api/player/win',
    config: {
      plugins: {
        'hapi-io': {
          event: 'player-won'
        }
      }
    },
    handler: req => {
      game.endGame(req)
    }
  })
})

server.register({
  register: require('hapi-io')
})

// Start the server
server.start((err) => {
  if (err) {
    throw err
  }
  console.log('Server running at:', server.info.uri)
})
